
// Parameters
var sitePath ="public";
var port = 2000;

// Libraries
var express = require('express');
var app = express();
var serv = require('http').Server(app);
//create http server
var Player = require('./servPlayer')


app.get('/', function(req,res){
	//returns game-page on GET requests to root dir
	res.sendFile( __dirname + '/' + sitePath + '/index.html');
});
app.use(express.static(__dirname + '/' + sitePath));
//allows serving of static files from public directory through express - needed for all phaser files

serv.listen(port);
console.log('server started');


var connectionCount =0;
var io = require('socket.io')(serv,{});
io.sockets.on('connection', onClientConnect);
var clients=[];

function onClientConnect(client){
	connectionCount++;
	console.log('Socket connection. Connected clients: ' + connectionCount);

	// Listen for client disconnected
	client.on('disconnect', onClientDisconnect);

	// Listen for new player message
	client.on('new player', onNewPlayer);

	// Listen for move player message
	client.on('move player', onMovePlayer);
	client.on('stand player', onStandPlayer);

	client.on('end game', onEndGame);
	client.on('give pickup', onGivePickup);
	client.on('consume pickup', onConsumePickup);

	client.on('open bridge', onOpenBridge);
	client.on('player velocity', onSetPlayerVelocity);
	client.on("swing player", onSwingPlayer);
	client.on("remove swing", onRemoveSwing);
};

function onEndGame(){
	this.broadcast.emit("end game");
};

function onSwingPlayer(data){
	this.broadcast.emit("swing player", {player:this.id, anchorX:data.anchorX, anchorY:data.anchorY});
};

function onRemoveSwing(){
	this.broadcast.emit("remove swing", {player:this.id});
};

function onSetPlayerVelocity(data){
	var movePlayer = findPlayer(this.id);

	// Player not found
	if (!movePlayer) {
		console.log('Player not found: ' + this.id);
		return;
	}

	this.broadcast.emit('player velocity', {x: data.x, y: data.y, id: movePlayer.getID(), velX:data.velX, velY:data.velY, direction:data.direction});
};

function onOpenBridge(data){
	this.broadcast.emit('open bridge', {lever: data.lever});
};


function onClientDisconnect(){
	var removePlayer = findPlayer(this.id);
	if(!removePlayer){
		console.log("Player not found");
		return;
	}
	var index = clients.indexOf(removePlayer);
	if (index > -1){
		clients.splice(index, 1);
		connectionCount = clients.length;
	}
	console.log("Socket disconnect. Connceted clients: " + connectionCount);
	this.broadcast.emit('player disconnect', {player: this.id});
};

function onNewPlayer(data){
	var newPlayer = new Player(this.id, data.x, data.y);
	console.log("New Player: " + newPlayer.getID());
	//console.log("id:" + newPlayer.getID() + " x:" +newPlayer.getX() + " y:"+newPlayer.getY());
	this.broadcast.emit('new player',  {id: newPlayer.getID(), x: newPlayer.getX(), y: newPlayer.getY()});

	for (var i = 0; i < clients.length; i++){
		this.emit("new player", {id: clients[i].getID(), x: clients[i].getX(), y: clients[i].getY()});
	}
	clients.push(newPlayer);

};

function onGivePickup(data){
	var pickupPlayer = findPlayer(this.id);
	if (!pickupPlayer) {
		console.log('Player not found: ' + this.id);
		return;
	}
	pickupPlayer.setActivePickup(data.pickup);
	this.broadcast.emit('give pickup', {player: pickupPlayer.getID(), pickup: pickupPlayer.getActivePickup()});
};

function onConsumePickup(data){
	var pickupPlayer = findPlayer(this.id);
	if (!pickupPlayer) {
		console.log('Player not found: ' + this.id);
		return;
	}

	this.broadcast.emit('consume pickup', {player: pickupPlayer.getID(), effectedPlayer: data.effectedPlayer});
};

function onMovePlayer(data){
	// Find player in array
	var movePlayer = findPlayer(this.id);

	// Player not found
	if (!movePlayer) {
		console.log('Player not found: ' + this.id);
		return;
	}

	// Update player position
	movePlayer.setX(data.x);
	movePlayer.setY(data.y);
	movePlayer.setDirection(data.direction);

	// Broadcast updated position to connected socket clients
	this.broadcast.emit('move player', {id: movePlayer.getID(), x: movePlayer.getX(), y: movePlayer.getY(), direction: movePlayer.getDirection()});
};

function onStandPlayer(){
	var sPlayer = findPlayer(this.id);

	// Player not found
	if (!sPlayer) {
		console.log('Player not found: ' + this.id);
		return;
	}
	this.broadcast.emit('stand player', {id: this.id});
};


function findPlayer (id) {
  var i;
  for (i = 0; i < clients.length; i++) {
    if (clients[i].getID() === id) {
      return clients[i];
    }
  }

  return false;
};
