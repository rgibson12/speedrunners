function servPlayer(id, x, y, game) {
	var x = x;
	var y = y;
	var game = game
	var direction = "";
	var activePickup = null;

/*************************************
						   getters
*************************************/
	this.getID = function(){
		return id;
	};

	this.getX = function(){
		return x;
	};

	this.getY = function(){
		return y;
	};

	this.getDirection = function(){
		return direction;
	};

	this.getActivePickup = function(){
		return activePickup;
	};

/*************************************
					   	setters
*************************************/
	this.setID = function(newID){
		id = newID;
	};

	this.setX = function(newX){
		x = newX;
	};

	this.setY = function(newY){
		y = newY;
	};

	this.setDirection = function(newDirection){
		direction = newDirection;
	};

	this.setActivePickup = function(pickupID){
		activePickup = pickupID;
	};

}
module.exports = servPlayer
