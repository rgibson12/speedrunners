function servBullet(id, x, y, game) {
	var x = x;
	var y = y;
	var game = game

/*************************************
						   getters
*************************************/
	this.getID = function(){
		return id;
	};

	this.getX = function(){
		return x;
	};

	this.getY = function(){
		return y;
	};

/*************************************
					   	setters
*************************************/
	this.setID = function(newID){
		id = newID;
	};

	this.setX = function(newX){
		x = newX;
	};

	this.setY = function(newY){
		y = newY;
	};

}
module.exports = servBullet
