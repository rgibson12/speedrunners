var game = new Phaser.Game("100%", "100%", Phaser.AUTO,  '', { preload: preload, create: create, update: update, render: render });

function preload () {
				game.load.tilemap('map1', 'assets/level12.json', null, Phaser.Tilemap.TILED_JSON);
				game.load.tilemap('map2', 'assets/level22.json', null, Phaser.Tilemap.TILED_JSON);
				game.load.tilemap('map3', 'assets/level23.json', null, Phaser.Tilemap.TILED_JSON);
				game.load.tilemap('map4', 'assets/level24.json', null, Phaser.Tilemap.TILED_JSON);
				game.load.tilemap('map5', 'assets/level25.json', null, Phaser.Tilemap.TILED_JSON);
				game.load.tilemap('map6', 'assets/challenging1.json', null, Phaser.Tilemap.TILED_JSON);
				game.load.tilemap('map7', 'assets/secondLevel.json', null, Phaser.Tilemap.TILED_JSON);
				game.load.tilemap('map8', 'assets/level3.json', null, Phaser.Tilemap.TILED_JSON);

				// Load the image 'level.png' and associate it in the cache as 'level'
				game.load.image('level', 'assets/level2.png');
				game.load.spritesheet('glowstick','assets/glowstick.png', 12, 4);
				game.load.spritesheet('door2','assets/door2.png', 26, 96);
				game.load.spritesheet('sDoor2','assets/sDoor2.png', 96, 21);
				game.load.audio('doorHum','assets/doorHum.mp3')
				game.load.image('pressurePlate', 'assets/pressurePlate.png');
				game.load.image('lvl1msg','assets/lvl1msg.png');
				game.load.spritesheet('lvl1msg21','assets/lvl1msg2.png', 443, 148);
				game.load.spritesheet('lvl1msg22','assets/lvl1msg22.png', 443, 148);
				game.load.spritesheet('lvl1msg23','assets/lvl1msg23.png', 443, 148);
				game.load.spritesheet('lvl1msg24','assets/lvl1msg24.png', 443, 148);
				game.load.spritesheet('lvl1msg25','assets/lvl1msg25.png', 443, 148);
				game.load.spritesheet('lvl1msg26','assets/lvl1msg26.png', 443, 148);
				game.load.image('door', 'assets/door.png');
				game.load.image('sDoor', 'assets/sDoor.png');
				game.load.image('end', 'assets/endpoint.png');

				game.load.image('pauseMenu','assets/pause.png');
				game.load.image('pauseLabel', 'assets/pauseLabel.png')

				// Load the spritesheet 'character.png', telling Phaser each frame is 30x48
			   game.load.spritesheet('character', 'assets/spritesheet2U.png', 30, 49);
			   game.load.spritesheet('mover', 'assets/movingPlate.png', 144, 6);
				game.load.spritesheet('crate', 'assets/crate.png', 48, 48);
				game.load.spritesheet('crateH', 'assets/crateH.png', 48, 48);
			   game.load.image("background", "assets/backdrop3.png");
			   game.load.image("backgroundLvl1", "assets/backdropLvl1.png");
			   game.load.image("backgroundLvl2", "assets/backdropLvl2.png");
			   game.load.image("backgroundLvl3", "assets/backdropLvl3.png");
			   game.load.image("backgroundLvl4", "assets/backdropLvl4.png");
				game.load.image("backgroundLvl5", "assets/backdropLvl5.png");
				//game.load.spritesheet('character', 'assets/spritesheet.png', 111, 49);
				game.load.spritesheet('startButton', 'assets/playBtn.png', 170, 47);

}

var player;
var socket;
var otherPlayers=[];

function create() {
	//console.log("Creating Game");
	var x = Math.floor((Math.random() * (8*48)) + (3*48));
	var y = (480);
	player = game.add.sprite(x,y, 'character');
	player.x = x;
	player.y = y;
	player.bringToTop();
	//this.socket = new SocketHandler(this);
	socket = io.connect();
	setEventHandlers();
	//this.socket.addNewPlayer(player);
}

function endGame() {
	console.log("Game Finished");
}

function update() {

}

function render() {

}
function quitGame(pointer) {

}

//SOCKET.IO
//*******************************************************************
function setEventHandlers() {
	// Socket connection successful
	socket.on('connect', onSocketConnected);

	// Socket disconnection
	//socket.on('disconnect', onSocketDisconnect)

	// New player message received
	socket.on('new player', onNewPlayer);

	// Player move message received
	//socket.on('move player', onMovePlayer)

	// Player removed message received
	//socket.on('remove player', onRemovePlayer)*/
}

function onSocketConnected(){
	console.log('New socket connection established');
	socket.emit('new player', {x:player.x, y:player.y});
}

function onSocketDisconnect(){
	console.log('Sockect connection closed');
}

function onNewPlayer(data){
	console.log('New Player connected');
	var newPlayer = game.add.sprite(data.x, data.y, 'character');
	otherPlayers.push(newPlayer);
}

function onRemovePlayer(data){
	console.log('Player disconnected: ' + data.id);
}
