function SocketHandler(game, factory) {
	var socket= io();
	var game = game;

	var factory = factory;

	this.setEventHandlers = function () {
		// Socket connection successful
		socket.on('connect', this.onSocketConnected);
		socket.on('end game', this.onEndGame);

		// Socket disconnection
		socket.on('player disconnect', this.onSocketDisconnect)

		// New player message received
		socket.on('new player', this.onNewPlayer);

		// Player move message received
		socket.on('move player', this.onMovePlayer);

		socket.on('stand player', this.onStandPlayer);

		socket.on('give pickup', this.onGivePickup);
		socket.on('consume pickup', this.onConsumePickup);

		socket.on('open bridge', this.onOpenBridge);
		socket.on('player velocity', this.onSetPlayerVelocity);
		socket.on("swing player", this.onSwingPlayer);
		socket.on("remove swing", this.onRemoveSwing);
		// Player removed message received
		//socket.on('remove player', onRemovePlayer)*/
	};

	this.onEndGame = function(){
		game.endTheGame();
	};

	this.onRemoveSwing = function(data){
		var player = findPlayer(data.player);
		player.setSwinging(false, 0, 0);
	};

	this.onSwingPlayer = function(data){
		var player = findPlayer(data.player);
		var anchorX = data.anchorX;
		var anchorY = data.anchorY;

		player.setSwinging(true, anchorX, anchorY);
	};

	this.onOpenBridge = function(data){
		var lever = game.levers[data.lever];
		if (lever.up){
			lever.bridge.body.angle = 0;
		} else {
			lever.bridge.body.angle= 90;
		}
		lever.up = !lever.up;
		lever.scale.x *= -1;
		lever.timer = game.time.now + 1000;
	};

	this.onGivePickup = function(data){
		var player = findPlayer(data.player);
		var pickup = game.pickups.pickups[data.pickup];

		player.setActivePickup(pickup);
		pickup.setTaken(true, player.getPlayerTex());
		game.pickups.removePickup(pickup.getID());
	};

	this.onConsumePickup = function(data){
		var player = findPlayer(data.player);
		var pickup = player.getActivePickup();

		switch (pickup.getType()) {
			case "toxicSpill":
				player = player.getPlayerTex();
				effectedPlayer = null;
				break;
			case "iceBlast":
				var effectedPlayer = findPlayer(data.effectedPlayer);
				if(!effectedPlayer){
					effectedPlayer=game.player;
				} else {
					effectedPlayer = effectedPlayer.getPlayerTex();
				}
				break;
			case 'crate':
				player = player.getPlayerTex();
				effectedPlayer = null;
				break;
			case 'hook':
				var effectedPlayer = findPlayer(data.effectedPlayer);
				var sourcePlayer = findPlayer(data.player);
				if (!sourcePlayer) {
					console.log('Player not found: ' + data.player);
					return;
				}
				console.log("SOURCE PLAYER IS: " + sourcePlayer.getID());

				//var hookToPlayer = findPlayer(data.effectedPlayer);

				if (!effectedPlayer) {
					effectedPlayer=game.player;
					console.log("hook to player is local player ");
				} else {
					console.log("hook to player FOUND ");
					effectedPlayer = hookToPlayer.getPlayerTex();
				}
				//effectedPlayer = false;
				//effectedPlayer = hookToPlayer;
				//effectedPlayer =effectedPlayer.getPlayerTex();
				player = player.getPlayerTex();
				break;
			default:
		}
		pickup.consumePickup({player: player, effectedPlayer: effectedPlayer, game: game});
	};

	this.disconnect = function(){
		socket.disconnect();
	};

	this.onUpdateTiles = function(data){
		game.map.replace(data.tile, data.changeTo);
	};

	this.onSocketConnected = function(){
		console.log('New socket connection established');
		//add local player to the server's playerlist - to broadcast to other clients
		//x and y don't matter - overwritten immediately by player move, just needed for server to process
		socket.emit('new player', {x:800, y:2500});
	};

	this.onSocketDisconnect = function(data){
		console.log('Player connection lost');
		var lostPlayer = findPlayer(data.player);
		if (!lostPlayer) {
	    console.log('Player not found: ' +  data.id);
	    return;
	  }
		lostPlayer.destroy()
		var index = game.otherPlayers.indexOf(lostPlayer);
		if (index >-1){
			game.otherPlayers.splice(index, 1);
			console.log("other player count:" + game.otherPlayers.length);
		}
		game.reset();
	};

	this.onNewPlayer = function(data){
		var newPlayer = factory.create("Player", {id: data.id, x: data.x, y: data.y, game: game})
		game.otherPlayers.push(newPlayer);
		console.log('New Player connected:' + newPlayer.getID());
		console.log("Other players :");
		for (var i = 0; i< game.otherPlayers.length; i++){
			console.log("|_>  " + game.otherPlayers[i].getID());
		}
	};

	this.onRemovePlayer = function(data){
		console.log('Player disconnected: ' + data.id);
	};

	this.onMovePlayer = function(data){
		var toMove = findPlayer(data.id);

	  // Player not found
	  if (!toMove) {
	    console.log('Player not found: ' +  data.id);
	    return;
	  }

	  // MOVE THIS INTO PLAYER OBJECT AND CALL
		toMove.move({dir: data.direction, x: data.x, y: data.y});

	};

	this.onSetPlayerVelocity = function(data){
		var toMove = findPlayer(data.id);

	  // Player not found
	  if (!toMove) {
	    console.log('Player not found: ' +  data.id);
	    return;
	  }
		//console.log("direction received: " + data.direction);
	  // MOVE THIS INTO PLAYER OBJECT AND CALL
		toMove.setVelocities({x: data.x, y: data.y, direction: data.direction, velX: data.velX, velY: data.velY});

	};

	this.onStandPlayer = function(data){
		var toStand = findPlayer(data.id);

	  // Player not found
	  if (!toStand) {
	    console.log('Player not found: ' +  data.id);
	    return;
	  }

		toStand.playAnimation("stand");
	};

	this.endGame = function(){
		socket.emit('end game');
	};

	this.movePlayer = function(data){
		socket.emit('move player', {x:data.x, y:data.y, direction:data.direction});
	};
	this.setPlayerVelocity = function(data){
		//console.log("emitting direction: " + data.direction);
		socket.emit('player velocity', {x: data.x, y: data.y, velX:data.velX, velY:data.velY, direction:data.direction});
	};
	this.standPlayer = function(){
		socket.emit('stand player');
	};

	this.consumePickup = function(data){
		socket.emit('consume pickup', {effectedPlayer: data.effectedPlayer});
	};


	this.openBridge = function(data){
		socket.emit('open bridge', {lever: data.lever});
	};

	this.givePickup = function(data){
			socket.emit('give pickup', {pickup: data.pickup});
	};

	this.swingPlayer = function(data){
			socket.emit("swing player", {anchorX:data.anchorX, anchorY:data.anchorY});
	};

	this.removeSwing = function(){
		socket.emit("remove swing");
	}
	//cant make instance function as need to use within socket callbacks which have different scope for "this" keyword->refers to socket
	var findPlayer = function(id) {
	  for (var i = 0; i < game.otherPlayers.length; i++) {
	    if (game.otherPlayers[i].getID() === id) {
	      return game.otherPlayers[i];
	    }
	  }

	  return false;
	};


}
