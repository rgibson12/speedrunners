
BasicGame.MainMenu = function (game) {
	this.startButton;
	this.socket;
};

BasicGame.MainMenu.prototype = {
	
	
	create: function () {
        //console.log("Creating menu state!");
		this.startButton = this.add.button(400, 370, 'startButton', this.startClick, this, 1,0);
		
		
	},

	update: function () {

	},
	
	socket: function() {
		return this.socket;
	},
	startClick: function(){
		this.state.start('Game');
		
	},

	resize: function (width, height) {

		//	If the game container is resized this function will be called automatically.
		//	You can use it to align sprites that should be fixed in place and other responsive display things.

	}

};
