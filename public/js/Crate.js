function Crate(x, y, game) {
	var x = x;
	var y = y;
	var lastX;
	var lastY;
	var game = game;
	var direction =""
	var crate = game.add.sprite(x,y, 'crate');
	crate.anchor.setTo(0.5,0.5);
	crate.x = x;
	crate.y = y;
	game.physics.enable(crate, Phaser.Physics.P2JS);
	crate.body.mass = 1000;
	crate.body.setCollisionGroup(game.crateCollisionGroup);
	crate.body.collides(game.playerCollisionGroup);
	crate.body.collides(game.groundCollisionGroup);
	game.pickupCrates.push(crate);

/*************************************
						   getters
*************************************/
	this.getX = function(){
		return x;
	};

	this.getY = function(){
		return y;
	};

/*************************************
					   	setters
*************************************/
	this.setX = function(newX){
		x = newX;
		crate.x = newX;
		crate.body.x = newX;
	};

	this.setY = function(newY){
		y = newY;
		crate.y = newY;
		crate.body.y = newY;
	};

	this.move = function(data){
		lastX = x;
		lastY = y;
		if (lastX != data.x || lastY != data.y){
			this.setX(data.x);
			this.setY(data.y);
			//this.moveAtSpeedX(data.x);
			//this.moveAtSpeedY(data.y);

		}
	}


	return this;

}
