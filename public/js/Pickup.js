function Pickup(id, x, y, game, type, manager) {
	var id = id;
	var x = x;
	var y = y;
	var game = game;
	var type=type;
	var manager = manager;
	var pickup;
	var taken = false;
	var token;
	var constraints=[];



	this.addToGame = function(){
		pickup = game.add.sprite(x, y, 'pickup');
		pickup.anchor.setTo(0.5, 0.5);
		pickup.bringToTop();
		pickup.x = x;
		pickup.y = y;
	};

	this.destroy = function(){
		pickup.destroy();
	};

	this.consumePickup = function (data){
		var player = data.player;
		var effectedPlayer = null;
		//var game = data.game;
		var effect;
		var ani= -1;
		var closestPlayer;
		console.log("consuming " + type);
		switch (type) {
      case 'toxicSpill':
				/*console.log("consuming " + type);
				ani = game.add.sprite (player.x -60, player.y, 'pickup_toxicSpill');
				ani.anchor.setTo(0.5, 0.5);
				ani.animations.add("pour",[1,2,3,4],1.5,false);
				ani.animations.play("pour");
				//ani.killOnComplete = true;
				ani.events.onAnimationComplete.add(
					function () {*/
						effect = game.add.sprite(player.x + 60, player.y+19, 'toxicSpill');
		        effect.anchor.setTo(1,0);
						effect.type="slow";
						effect.multipler=0.3;
						game.inGameEffects.push(effect);
						//this.destroy();
					/*}, this);*/
        break;

			case 'iceBlast':
				if (!data.effectedPlayer){
					closestPlayer=this.closestPlayer(player);
					var closestPlayerTex=closestPlayer.getPlayerTex();
					console.log("closest player:" + closestPlayer.getID());
					effectedPlayer = closestPlayer.getID();
				} else {
					closestPlayerTex=data.effectedPlayer;
				}


					closestPlayerTex.loadTexture("iceBlock");
					closestPlayerTex.canMove = false;
					game.time.events.add(Phaser.Timer.SECOND * 3, function(){
						closestPlayerTex.loadTexture("character");
						closestPlayerTex.canMove = true;
						console.log("unfreezing");
					}, this);


				console.log("player to effect: " + effectedPlayer);
				break;

			case 'crate':
				var crate = factory.create("Crate", {x:player.x - 30, y: player.y+9, game: game});
				//game.crates.push(crate);
				break;

			case 'hook':
			if (!data.effectedPlayer){
				console.log("no player emitted");
				var closestPlayer=this.closestPlayer(player);
				var closestPlayerTex = closestPlayer.getPlayerTex();
				//console.log("closest player:" + closestPlayer.getID());
				effectedPlayer = closestPlayer.getID();
			}else {
				closestPlayerTex=data.effectedPlayer;
			}
			/*closestPlayerTex.scale.setTo(0.5, 0.5);
			//console.log(closestPlayerTex.body.data);
			//closestPlayerTex.body.data.shapes[0].width*0.5;
			//closestPlayerTex.body.data.shapes[0].height*0.5;
			//console.log(closestPlayerTex.body.data);
			game.time.events.add(Phaser.Timer.SECOND * 3, function(){
				closestPlayerTex.scale.setTo(1,1);
				//closestPlayerTex.body.data.scale.x = 0.5;
				//closestPlayerTex.body.data.scale.y = 0.5;
			}, this);*/
				var lastMass = player.body.mass;
				player.body.mass = 1000;
				constraints.push(game.physics.p2.createDistanceConstraint(closestPlayerTex.body, player.body, 0, [0,0], [0,0], 1000));
				//game.hook = game.physics.p2.createSpring(closestPlayerTex.body, player.body, 0, 60, 1);
				//game.hookLine = new Phaser.Line(player.x, player.y, closestPlayerTex.x, closestPlayerTex.y);
				//game.hookedPlayer = closestPlayer;
        //game.drawHookLine = true;
				//closestPlayer.canMove = false;
				//player.canMove = false;
				var mag = player.addChild(game.make.sprite(10,-20, 'magnet'));
				mag.animations.add("ani",[1,2,3,4,5],10,true);
				mag.animations.play("ani");
				game.time.events.add(Phaser.Timer.SECOND * 3, function(){
					for (var i =0;i<constraints.length; i++){
						game.physics.p2.removeConstraint(constraints[i]);
					}
					player.body.mass = lastMass;
					mag.destroy();
					//closestPlayer.canMove = true;
					//player.canMove = true;
				}, this);

				break;
      default:
				effect = -1;
				break;
				//return ani;
    }
		//player holds an activePickup that other clients will be able to read - no need to emit the pickup to consume.
		token.destroy();
		token = null;
		var ret = {player: player, effectedPlayer: effectedPlayer};
		return ret
		//manager.socket.consumePickup({player: player, effectedPlayer: effectedPlayer});
	};

	this.closestPlayer = function(player){
		var closestPlayer = game.otherPlayers[0];
		var closestDeltaX = Math.abs(player.x - closestPlayer.getX());
		var closestDeltaY = Math.abs(player.y - closestPlayer.getY());

		for (var i =1; i <game.otherPlayers.length; i++){
			var curPlayer = game.otherPlayers[i];
			var curDeltaX = Math.abs(player.x - curPlayer.getX());
			var curDeltaY = Math.abs(player.y - curPlayer.getY());

			if (curDeltaY+curDeltaX < closestDeltaY+closestDeltaX){
				closestPlayer=curPlayer;
				closestDeltaX = curDeltaX;
				closestDeltaY = curDeltaY;
			}
		}
		return closestPlayer;
	};

/*************************************
						   getters
*************************************/
	this.getID = function(){
		return id;
	};

	this.getX = function(){
		return x;
	};

	this.getY = function(){
		return y;
	};

	this.getPickup = function(){
		return pickup;
	};

	this.getTaken = function(){
		return taken;
	};

	this.getType = function(){
		return type;
	}

/*************************************
					   	setters
*************************************/

	this.setX = function(newX){
		x = newX;
		pickup.x = newX;
	};

	this.setY = function(newY){
		y = newY;
		pickup.y = newY;
	};

	this.setTaken = function(newTaken, player){
		taken = newTaken;
		if(player){
			if (token){
				token.setVisibie(false);
				player.removeChild(token);
				token.destroy();
			}
			token = player.addChild(game.make.sprite(-33,-33, 'token_'+type));
		}
	};

	this.playAnimation = function(ani){
		pickup.animations.play(ani);
	};

	this.removeFromGame = function(){
		pickup.destroy();
	};

	this.consume = function(pickup){

	};

	return this;

}
