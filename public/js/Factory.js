function Factory() {
  var objectsCreated = 0;
  var playersCreated = 0;


  this.create = function(toCreate, params){
    var newObject=-1;
    switch(toCreate) {
      case "Crate":
        newObject = this.createCrate(params);
        break;
      case "Player":
        newObject = this.createPlayer(params);
        break;
      case "Pickup":
        newObject = this.createPickup(params);
        break;
      case "Bullet":
        newObject = this.createBullet(params);
        break;
      case "BulletManager":
        newObject = this.createBulletManager(params);
        break;
      case "PickupManager":
        newObject = this.createPickupManager(params);
        break;
      case "SocketHandler":
        newObject = this.createSocketHandler(params);
        break;
      case "GrapplingHook":
        newObject = this.createGrapplingHook(params);
        break;
      default:
        console.log("Cannot create: " + toCreate + " - not a recognized class.")
    }
    if (newObject!=-1){
      return newObject;
    }
  };

  this.createCrate = function(params){
    var x = params.x;
    var y = params.y;
    var game = params.game;

    var newCrate = new Crate(x, y, game);
    if (newCrate){
      objectsCreated++;
      return newCrate;
    }
  };

  this.createGrapplingHook = function(params){
    var player = params.player;
    var socket = params.socket;
    var game = params.game;

    var newHook = new GrapplingHook(game, player, socket);
    if (newHook){
      objectsCreated++;
      return newHook;
    }
  };

  this.createPlayer = function(params){
    var id = params.id;
    var x = params.x;
    var y = params.y;
    var game = params.game;

    var newPlayer = new Player(id, x, y, game);
    if (newPlayer){
      objectsCreated++;
      playersCreated++;
      return newPlayer;
    }
  };

  this.createPickup = function(params){
    var id = params.id;
    var x = params.x;
    var y = params.y;
    var game = params.game;
    var type = params.type;
    var manager = params.manager;

    var newPickup = new Pickup(id, x, y, game, type, manager);
    if (newPickup){
      objectsCreated++;
      return newPickup;
    }
  };

  this.createBullet = function(params){
    var id = params.id;
    var x = params.x;
    var y = params.y;
    var game = params.game;

    var newBullet = new Bullet(id, x, y, game);
    if (newBullet){
      objectsCreated++;
      return newBullet;
    }
  };

  this.createSocketHandler = function(params){
    var game = params.game;
    var factory = params.factory;

    var newSocketHandler = new SocketHandler(game, factory);
    if (newSocketHandler){
      objectsCreated++;
      return newSocketHandler;
    }
  };

  this.createBulletManager = function(params){
    var game = params.game;
    var factory = params.factory;
    var socket = params.socket;

    var newBulletManager = new BulletManager(game, factory, socket);
    if (newBulletManager){
      objectsCreated++;
      return newBulletManager;
    }
  };

  this.createPickupManager = function(params){
    var game = params.game;
    var factory = params.factory;
    var socket = params.socket;
    var newPickupManager = new PickupManager(game, factory, socket);
    if (newPickupManager){
      objectsCreated++;
      return newPickupManager;
    }
  };

  return this;

}
