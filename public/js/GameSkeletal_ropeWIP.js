var game = new Phaser.Game("100%", "100%", Phaser.AUTO,  '', { preload: preload, create: create, update: update, render: render });

function preload () {
	//allow continuous rendering when window loses focus
	game.stage.disableVisibilityChange = true;
	game.load.tilemap('map', 'assets/levelDiags.json', null, Phaser.Tilemap.TILED_JSON);
	game.load.physics("slantRdata", 'assets/slantR.json');
	game.load.image('base', 'assets/levelL.png');
	game.load.spritesheet('character', 'assets/spritesheet_new.png', 34, 50);
	game.load.spritesheet('iceBlock', 'assets/iceBlock.png', 34, 50);
	game.load.spritesheet('crate', 'assets/crate.png', 31, 31);
	game.load.spritesheet('rope', 'assets/rope.png',5, 100);
	game.load.image('gun', 'assets/gun.png');
	game.load.spritesheet('bridge', 'assets/bridgeL.png', 240, 48);
	game.load.spritesheet('speedupR', 'assets/speedR.png', 144, 48);
	game.load.spritesheet('speedupL', 'assets/speedL.png', 144, 48);
	game.load.spritesheet('lever','assets/lever.png',27,24);
	game.load.spritesheet('slantR', 'assets/slantR.png', 48, 48);
	game.load.spritesheet('toxicSpill', 'assets/toxicSpill.png', 144, 54);
	game.load.spritesheet('pickup_toxicSpill', 'assets/pickup_toxicSpill2.png', 29, 45);
	game.load.spritesheet('pickup_iceBlast', 'assets/pickup_iceBlast.png', 19, 19);
	game.load.spritesheet('pickup_crate', 'assets/pickup_crate.png', 19, 19);
	game.load.spritesheet('pickup_hook', 'assets/pickup_hook.png', 19, 29);

}
//globals
var player;
var socket;
var factory;
var cursors;
var pickupUseBtn;
var pickups;

var otherPlayers=[];
var crates=[];
var bridge1;
var bridges=[];
var lever1;
var levers=[];

var rope;
var gun;

//remove later **********
var bullet;
var fireRate = 100;
var nextFire = 0;
var bulletCount = 0;
var bulletId = 0;
//***********************
var map;
var layer;
var slantLayer;
var slants;
var playerCollisionGroup;
var groundCollisionGroup;
var crateCollisionGroup;
var tiles = [];
var tempTileBool = false;

var text;
var drawHookLine = false;
var hookLine;

function create() {
	//factory instance
	factory = new Factory();

	//bullet controller instance
	//bullet = factory.create('BulletManager', {game: game, factory: factory, socket: socket});

	//game world setup
	map = game.add.tilemap('map');
	map.addTilesetImage('base');
	layer = map.createLayer('Tile Layer 1');
	slantLayer = map.createLayer('Tile Layer 2');
	layer.resizeWorld();
	map.setCollisionBetween(2, 5);
	game.physics.startSystem(Phaser.Physics.P2JS);
	game.physics.p2.setImpactEvents(true);
	game.physics.p2.gravity.y = 1000;
	tiles = game.physics.p2.convertTilemap(map, layer);
	slants = game.physics.p2.convertCollisionObjects(map, "diagonals");

	rope = game.add.sprite(7, -2, 'rope');
  rope.anchor.setTo(0.5, 0);
	//game.physics.p2.enable(rope);
	//rope.static = true;
  rope.visible = false;
	rope.target = new Phaser.Point();
	rope.progress = 0;
	rope.shottime = 0;

	//collision models
	playerCollisionGroup = game.physics.p2.createCollisionGroup();
	groundCollisionGroup = game.physics.p2.createCollisionGroup();
	crateCollisionGroup = game.physics.p2.createCollisionGroup();
	game.playerCollisionGroup = playerCollisionGroup;
	game.groundCollisionGroup = groundCollisionGroup;
	game.physics.p2.updateBoundsCollisionGroup();
	for (var i =0; i<tiles.length; i++){
		tiles[i].localIndexer = i;
		tiles[i].setCollisionGroup(groundCollisionGroup);
		tiles[i].collides(playerCollisionGroup);
		tiles[i].collides(crateCollisionGroup);
	}
	for (var i =0; i<slants.length; i++){
		//objects[i].localIndexer = i;
		slants[i].setCollisionGroup(groundCollisionGroup);
		slants[i].collides(playerCollisionGroup);
		slants[i].collides(crateCollisionGroup);
	}
	bridge1 = game.add.sprite(250+768+148+10, 2616, 'bridge');
		game.physics.p2.enable(bridge1);
		bridge1.body.data.gravityScale = 0;
		bridge1.body.setCollisionGroup(groundCollisionGroup);
		bridge1.body.collides(playerCollisionGroup);
		bridge1.body.collides(crateCollisionGroup);
		bridge1.body.mass = 1000000;
		bridges.push(bridge1);

	lever1 = game.add.sprite(250+768+148+10+140, 2616-48+12, 'lever');
		lever1.bridge = bridge1;
		lever1.up = false;
		lever1.timer=0;
		lever1.anchor.setTo(0.5,0.5);
		levers.push(lever1);

	game.levers = levers;
	game.bridges=bridges;
	//local player setup
	var x = (800);
	var y = (2500);
	player = game.add.sprite(x,y, 'character');

	player.x = x;
	player.y = y;
	player.canJump = false;
	player.canMove = true;

	//game.physics.enable(player, Phaser.Physics.P2JS);
	game.physics.p2.enable(player, true);
  player.body.collideWorldBounds = false;
	player.checkWorldBounds = true;
	player.events.onOutOfBounds.add(function(){
		player.body.x = 800;
		player.body.y = 2500;
		console.log("resetting player");
	},this);
	player.animations.add("runR",[1,2,3,4,5,6],45,true);
	player.animations.add("runL",[12,11,10,9,8,7],45,true);
	player.animations.add("freeze",[13]);
	player.animations.add("stand",[0]);
	player.direction = "";
	player.body.fixedRotation = true;
	player.body.setCollisionGroup(playerCollisionGroup);
	player.body.collides(groundCollisionGroup, hitGround, this);
	player.body.collides(crateCollisionGroup);
	player.speedMultipler = 1;
	player.activePickup = null;
	//player.ropeScale= new Phaser.Point();
	game.camera.follow(player);

	gun = game.add.sprite(15, 10, 'gun');
  gun.anchor.setTo(0.5, 0.5);
  player.addChild(gun);

	//for reference
	game.map = map;
	game.player = player;
	game.otherPlayers = otherPlayers;
	game.hookLine = hookLine;
	game.drawHookLine = drawHookLine;
	game.hookedPlayer = null;
	game.usedHookPlayer = player;
	game.hook = null;
  //input setup
	cursors = game.input.keyboard.createCursorKeys();
	pickupUseBtn = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);

	//socket manager
	socket = factory.create('SocketHandler', {game: game, factory: factory});
	socket.setEventHandlers();

	//pickup manager
	pickups = factory.create('PickupManager', {game: game, factory: factory, socket: socket});
	pickups.setupPickups();
	game.pickups = pickups;
	game.crates = crates;

	game.playerCollisionGroup=playerCollisionGroup;
	game.groundCollisionGroup=groundCollisionGroup;
	game.crateCollisionGroup=crateCollisionGroup;
	//game.input.onDown.add(replaceTiles, this);

	var style = { font: "65px Arial", fill: "#ff0044", align: "center" };

	text = game.add.text(player.x, player.y-200, "-LineRunners-\nWaiting for another\nplayer to connect", style);

	text.anchor.set(0.5);

	var speed1 = game.add.sprite(250+768+148+10-500 + 60, player.y+45, 'speedupR');
	speed1.animations.add("runR",[2,1,0],3,true);
	speed1.animations.play("runR");
		speed1.anchor.setTo(1,0);
		speed1.type="boost";
		speed1.duration=1.75;
		speed1.direction = "r";
		//speed1.sendToBack();
		speed1.multipler=1.5;
		pickups.pickupEffects.push(speed1);
			player.bringToTop();

	game.input.onUp.add(shootRope, this);

}

function shootRope() {
	if (rope.visible) {
		rope.visible = false;
	} else {
		console.log("making rope visible");
		rope.shottime = game.time.time;
		rope.target.setTo(game.input.x, game.input.y);
		console.log("target = " + rope.target.x + ", " + rope.target.y);
		rope.visible = true;
	}
}

function hitGround(sprite, tile) {
	player.canJump = true;
}

function replaceTiles() {
	if(tempTileBool){
		map.replace(2, 1);
		map.setCollisionBetween(2, 4);
		socket.updateTiles({tile: 2, changeTo: 1});
	} else {
		map.replace(1, 2);
		map.setCollisionBetween(2, 4);
		socket.updateTiles({tile: 1, changeTo: 2});
	}
	tempTileBool = !tempTileBool;

}



function endGame() {
	console.log("Game Finished");
}

function checkLevers(){
	var boundsA = player.getBounds();
	for (var i =0; i <levers.length; i++){
		var lever = levers[i];
		if (lever.timer < game.time.now){
	  	var boundsB = lever.getBounds();

	    if (Phaser.Rectangle.intersects(boundsA, boundsB) || Phaser.Rectangle.intersects(boundsB, boundsA)){
				console.log("player on lever");
				openBridge(i);
				socket.openBridge({lever: i});
			}
		}
	}
}

function openBridge(leverInd){
	var lever = levers[leverInd];
	if (lever.up){
		while(lever.bridge.body.angle > 0){
			lever.bridge.body.angle -=0.2;
		}
		//lever.bridge.body.angle = 0;
	} else {
		while(lever.bridge.body.angle < 90){
			lever.bridge.body.angle +=0.2;
		}
	}
	lever.up = !lever.up;
	lever.scale.x *= -1;
	lever.timer = game.time.now + 1000;
}

function update() {

	if (rope.visible) {
		//	console.log("drawing rope");
      rope.x = gun.x + 5;
      rope.y = gun.y + 6;
      rope.angle = game.math.angleBetween(rope.x, rope.y, rope.target.x, rope.target.y) - Math.PI/2;
      rope.theLength = 30;
		//	console.log("rope length:" + rope.theLength);
      rope.scale.y = (game.math.distance(player.body.x, player.body.y, rope.target.x, rope.target.y)/100) * rope.theLength;
		//	console.log("rope scale:" + rope.scale.y);
    } else {
     rope.target.setTo(game.input.x, game.input.y);
      if (game.input.x < player.body.x) {
        player.scale.x = -1;
        gun.rotation = -game.math.angleBetween(player.body.x, player.body.y, rope.target.x, rope.target.y) + Math.PI;
      } else {
        player.scale.x = +1;
        gun.rotation = game.math.angleBetween(player.body.x, player.body.y, rope.target.x, rope.target.y);
      }
    }

	if(game.otherPlayers.length < 1){
		console.log("game not ready");
	} else {
		if(text){
			game.world.remove(text);
			text = false;
		}
		player.body.velocity.x = 0;
		pickups.checkPickups(player);
		pickups.checkSlowEffects(player);
		checkLevers();
		if(player.canMove){
			 if (cursors.left.isDown)
			 {
					 player.body.velocity.x = -400 * player.speedMultipler;
					 player.animations.play("runL");
					 player.direction = "l";
					 //player.x-=5;
					 //socket.movePlayer({ x: player.x, y: player.y, direction: player.direction });
			 }
			 else if (cursors.right.isDown)
			 {
					 player.body.velocity.x = 400 * player.speedMultipler;
					 	//player.x+=5;
					 player.animations.play("runR");
					 player.direction = "r";
					 //socket.movePlayer({ x: player.x, y: player.y, direction: player.direction });
			 }
			 else{
				 player.animations.play("stand");
				 //socket.standPlayer();
			 }
			 if (cursors.up.isDown) {
				 if(player.canJump){
				 		player.body.velocity.y = -400;
						player.canJump = false;
				}
			 }
			 if (game.input.activePointer.isDown)
		  	{
					if (game.time.now > nextFire && bulletCount <= 100){
						//bullet.fire(player.x, player.y);
						//bulletCount++;
					}
		    }
				if (pickupUseBtn.isDown) {
					if(player.canJump){
						if(player.activePickup != null){
							pickups.consumePickup(player);
							//socket.consumePickup();
						}
					}
				}
			//if(player.body.velocity.y != 0 || player.body.velocity.x != 0){
				//socket.movePlayer({ x: player.x, y: player.y, direction: player.direction });


			//} else {
				//socket.standPlayer();
			//}
		}
			socket.setPlayerVelocity({x: player.x, y: player.y, velX: player.body.velocity.x, velY: player.body.velocity.y, direction: player.direction});
		for (var i =0; i < game.crates.length; i++){
			//socket.moveCrate({id: i, x: game.crates[i].getX(), y: game.crates[i].getY()});
		}
		if (bulletCount > 0){
			socket.moveBullet({ id: bulletId, x: bullet.getBulletX(), y:bullet.getBulletY()});
		};
	}
	if(game.drawHookLine){
		game.hookLine.setTo(game.usedHookPlayer.x, game.usedHookPlayer.y, game.hookedPlayer.getX(), game.hookedPlayer.getY());
		pickups.checkHook(game.usedHookPlayer, game.hookedPlayer.getPlayerTex());
	}
}

function render() {
	if (game.drawHookLine){
		game.debug.geom(game.hookLine);
	}
}
function quitGame(pointer) {

}

/*
//SOCKET.IO
//*******************************************************************
function setEventHandlers() {
	// Socket connection successful
	socket.on('connect', onSocketConnected);

	// Socket disconnection
	//socket.on('disconnect', onSocketDisconnect)

	// New player message received
	socket.on('new player', onNewPlayer);

	// Player move message received
	//socket.on('move player', onMovePlayer)

	// Player removed message received
	//socket.on('remove player', onRemovePlayer)
}

function onSocketConnected(){
	console.log('New socket connection established');
	socket.emit('new player', {x:player.x, y:player.y});
}

function onSocketDisconnect(){
	console.log('Sockect connection closed');
}

function onNewPlayer(data){
	console.log('New Player connected');
	var newPlayer = game.add.sprite(data.x, data.y, 'character');
	otherPlayers.push(newPlayer);
}

function onRemovePlayer(data){
	console.log('Player disconnected: ' + data.id);
}*/
