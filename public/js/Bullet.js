function Bullet(id, x, y, game) {
  var id = id;
  var x = x;
  var y = y;
  var game = game;

  var bullet = game.add.sprite(x, y, 'bullet');
  bullet.bringToTop();
  bullet.x = x;
  bullet.y = y;
  game.physics.enable(bullet, Phaser.Physics.ARCADE);
  bullet.body.allowGravity = false;
  bullet.checkWorldBounds = true;
  bullet.outOfBoundsKill = true;


  this.getID = function(){
    return id;
  };

  this.getX = function(){
    return x;
  };

  this.getY = function(){
    return y;
  };

  this.setID = function(newID){
		id = newID;
	};

	this.setX = function(newX){
		x = newX;
		bullet.x = newX;
		bullet.body.x = newX;
	};

	this.setY = function(newY){
		y = newY;
		bullet.y = newY;
		bullet.body.y = newY;
	};

  this.fire = function(){
    game.physics.arcade.moveToPointer(bullet, 600);
  };

	return this;

}
