function BulletManager(game, factory, socket) {
  this.bullets =[];
  this.game = game;
  this.factory = factory;
  this.socket = socket;
  this.bullet = false;

  this.idInc = 0;

  this.fire = function(x, y){

      this.bullet = game.add.sprite(x, y, 'bullet');
      this.bullet.bringToTop();
      this.bullet.x = x;
      this.bullet.y = y;
      game.physics.enable(this.bullet, Phaser.Physics.ARCADE);
      this.bullet.body.allowGravity = false;
      this.bullet.checkWorldBounds = true;
      this.bullet.outOfBoundsKill = true;

      this.bullets.push(this.bullet);
      this.socket.newBullet({id: this.idInc, x: this.bullet.x, y: this.bullet.y });

      this.idInc++;

      game.physics.arcade.moveToPointer(this.bullet, 600);

    return this.idInc-1;
  };

  this.update = function(){
    for (var i in this.bullets){
      game.socket.moveBullet({ x: i.x, y: i.y });
    }
  };

  this.getBulletX = function(){
    return this.bullet.x;
  };
  this.getBulletY = function (){
    return this.bullet.y;
  };

	return this;

}
