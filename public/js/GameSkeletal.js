var game = new Phaser.Game(window.innerWidth * window.devicePixelRatio, window.innerHeight * window.devicePixelRatio, Phaser.AUTO,  '', { preload: preload, create: create, update: update, render: render });

function preload () {
	//allow continuous rendering when window loses focus
	game.stage.disableVisibilityChange = true;
	game.load.tilemap('map', 'assets/level2.json', null, Phaser.Tilemap.TILED_JSON);
	game.load.physics("slantRdata", 'assets/slantR.json');
	game.load.image('base', 'assets/levelL.png');
	game.load.image('endpoint', 'assets/endpoint.png');
	game.load.image('token_crate', 'assets/token_crate.png');
	game.load.image('token_toxicSpill', 'assets/token_toxicspill.png');
	game.load.image('token_iceBlast', 'assets/token_iceblast.png');
	game.load.image('token_hook', 'assets/token_magnet.png');
	game.load.spritesheet('character', 'assets/spritesheet_final.png', 34, 50);
	game.load.spritesheet('magnet', 'assets/magnet.png', 56, 40);
	game.load.spritesheet('iceBlock', 'assets/iceBlock.png', 34, 50);
	game.load.spritesheet('crate', 'assets/crate.png', 31, 31);
	game.load.spritesheet('bridge', 'assets/bridgeL.png', 240, 48);
	game.load.image('hookAnchor', 'assets/anchor.png');
	game.load.image('pickup', 'assets/pickup.png');
	game.load.spritesheet('hookSurf', 'assets/tempHookSurf.png', 48, 8);
	game.load.spritesheet('speedupR', 'assets/speedR.png', 144, 48);
	game.load.spritesheet('speedupL', 'assets/speedL.png', 144, 48);
	game.load.spritesheet('lever','assets/lever.png',27,24);
	game.load.spritesheet('slantR', 'assets/slantR.png', 48, 48);
	game.load.spritesheet('toxicSpill', 'assets/toxicSpill.png', 144, 54);


}
//globals
var player;
var socket;
var factory;
var btnRight;
var btnLeft;
var btnJump;
var pickupUseBtn;
var pickups;
var pickupCrates = [];
var gameOver = false;
var drawSwing = false;
var swing = null;

var otherPlayers=[];
var speedBoosts=[];
var bridge1;
var bridges=[];
var lever1;
var levers=[];
var inGameEffects = [];

var hookSurfs=[];

var map;
var layer;
var slantLayer;
var slants;
var playerCollisionGroup;
var groundCollisionGroup;
var crateCollisionGroup;
var grappleHookCollisionGroup;
var hookSurfaceCollisionGroup;
var endpointCollisionGroup;

var tiles = [];
var tempTileBool = false;
var winLose = " LOSE";
var playerLine = new Phaser.Line(0,0,0,0);
var text;
var coverUp;
//var drawHookLine = false;
var hookLine;

//var tempHookSurf;

function create() {
	//factory instance
	factory = new Factory();


	//game world setup
	map = game.add.tilemap('map');
	map.addTilesetImage('base');
	layer = map.createLayer('Tile Layer 1');
	slantLayer = map.createLayer('Tile Layer 2');
	layer.resizeWorld();
	map.setCollisionBetween(2, 5);
	game.physics.startSystem(Phaser.Physics.P2JS);
	game.physics.startSystem(Phaser.Physics.ARCADE);
	game.physics.p2.setImpactEvents(true);
	game.physics.p2.gravity.y = 1000;
	tiles = game.physics.p2.convertTilemap(map, layer);
	slants = game.physics.p2.convertCollisionObjects(map, "diagonals");


	playerCollisionGroup = game.physics.p2.createCollisionGroup();
	groundCollisionGroup = game.physics.p2.createCollisionGroup();
	crateCollisionGroup = game.physics.p2.createCollisionGroup();
	grappleHookCollisionGroup = game.physics.p2.createCollisionGroup();
	hookSurfaceCollisionGroup = game.physics.p2.createCollisionGroup();
	endpointCollisionGroup = game.physics.p2.createCollisionGroup();
	game.playerCollisionGroup = playerCollisionGroup;
	game.groundCollisionGroup = groundCollisionGroup;
	game.physics.p2.updateBoundsCollisionGroup();
	for (var i =0; i<tiles.length; i++){
		tiles[i].localIndexer = i;
		tiles[i].setCollisionGroup(groundCollisionGroup);
		tiles[i].collides(playerCollisionGroup);
		tiles[i].collides(crateCollisionGroup);
		tiles[i].collides(grappleHookCollisionGroup);
	}
	for (var i =0; i<slants.length; i++){
		//objects[i].localIndexer = i;
		slants[i].setCollisionGroup(groundCollisionGroup);
		slants[i].collides(playerCollisionGroup);
		slants[i].collides(crateCollisionGroup);
	}

setupBridges();


	//local player setup
	var x = (window.outerWidth/2);
	var y = (2500);
	player = game.add.sprite(x,y, 'character');
	player.x = x;
	player.y = y;
	player.canJump = false;
	player.canMove = true;
	game.physics.enable(player, Phaser.Physics.P2JS);
  player.body.collideWorldBounds = false;
	player.checkWorldBounds = true;
	player.events.onOutOfBounds.add(function(){
		player.body.x = x;
		player.body.y = y;
		console.log("resetting player");
	},this);
	player.animations.add("run",[1,2,3,4,5,6],45,true);
	//player.animations.add("runL",[12,11,10,9,8,7],45,true);
	player.animations.add("freeze",[13]);
	player.animations.add("stand",[0]);
	player.direction = "r";
	player.body.fixedRotation = true;
	player.body.setCollisionGroup(playerCollisionGroup);
	player.body.collides(groundCollisionGroup, hitGround, this);
	player.body.collides(crateCollisionGroup);
	player.body.collides(endpointCollisionGroup, endGame, this);
	player.speedMultipler = 1;
	player.activePickup = null;
	player.swinging=false;
	player.swing = swing;
	game.camera.follow(player);


	//hookable surfaces setup
	var surf = game.add.sprite(551.5-200+(7.5*48)+8, player.y-200+96 ,"hookSurf");
	surf.scale.x = 4;
	hookSurfs.push(surf);
	surf = game.add.sprite(551.5-200+(7.5*48)+8-(6.5*48), player.y-200+96+(6*48) ,"hookSurf");
	surf.scale.x = 3;
	hookSurfs.push(surf);
	surf = game.add.sprite(551.5-200+(7.5*48)+8+(14.5*48), player.y-200+96-(4*48) ,"hookSurf");
	surf.scale.x = 7;
	hookSurfs.push(surf);
	surf = game.add.sprite(551.5-200+(7.5*48)+8+(14.5*48), player.y-200+96-(4*48)-(6*48) ,"hookSurf");
	surf.scale.x = 7;
	hookSurfs.push(surf);
	surf = game.add.sprite(551.5-200+(7.5*48)+8+(14.5*48)+(7.5*48), player.y-200+96-(4*48)-(6*48)+(24*48),"hookSurf");
	surf.scale.x = 6;
	hookSurfs.push(surf);
	surf = game.add.sprite(551.5-200+(7.5*48)+8+(14.5*48)+(38.5*48), player.y-200+96-(4*48)-(6*48)+(24*48),"hookSurf");
	surf.scale.x = 6;
	hookSurfs.push(surf);
	surf = game.add.sprite(551.5-200+(7.5*48)+8+(29.5*48)+(31.5*48), player.y-200+96-(4*48)-(6*48)+(4*48),"hookSurf");
	surf.scale.x = 4;
	hookSurfs.push(surf);
	surf = game.add.sprite(551.5-200+(7.5*48)+8+(29.5*48)+(36.5*48), player.y-200+96-(4*48)-(6*48)+(6*48),"hookSurf");
	surf.scale.x = 4;
	hookSurfs.push(surf);
	surf = game.add.sprite(551.5-200+(7.5*48)+8+(29.5*48)+(36*48), player.y-200+96-(4*48)-(6*48)-(10*48),"hookSurf");
	surf.scale.x = 9
	hookSurfs.push(surf);
	for (var hookSurfCounter =0; hookSurfCounter<hookSurfs.length; hookSurfCounter++){
		game.physics.p2.enable(hookSurfs[hookSurfCounter]);
		hookSurfs[hookSurfCounter].body.static = true;
		hookSurfs[hookSurfCounter].body.setCollisionGroup(hookSurfaceCollisionGroup);
		hookSurfs[hookSurfCounter].body.collides(grappleHookCollisionGroup);
	}

	var endpoint = game.add.sprite(35*48, player.y+45-(24*48), 'endpoint');
	game.physics.p2.enable(endpoint);
	endpoint.body.static = true;
	endpoint.body.setCollisionGroup(endpointCollisionGroup);
	endpoint.body.collides(playerCollisionGroup);

	//for external reference
	game.map = map;
	game.player = player;
	game.otherPlayers = otherPlayers;
	game.hookLine = hookLine;
	//game.drawHookLine = drawHookLine;
	game.hookedPlayer = null;
	game.usedHookPlayer = player;
	game.hook = null;
	game.playerCollisionGroup=playerCollisionGroup;
	game.groundCollisionGroup=groundCollisionGroup;
	game.crateCollisionGroup=crateCollisionGroup;
	game.hookSurfaceCollisionGroup = hookSurfaceCollisionGroup;
	game.endpointCollisionGroup = endpointCollisionGroup;
	game.inGameEffects = inGameEffects;
	game.levers = levers;
	game.bridges=bridges;
	game.drawSwing = drawSwing;
	game.pickupCrates = pickupCrates;
  //input setup
	btnRight = game.input.keyboard.addKey(Phaser.Keyboard.D);
	btnLeft = game.input.keyboard.addKey(Phaser.Keyboard.A);
	btnJump = game.input.keyboard.addKey(Phaser.Keyboard.W);
	pickupUseBtn = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);

	//socket manager
	socket = factory.create('SocketHandler', {game: game, factory: factory});
	socket.setEventHandlers();
	player.grapplingHook = factory.create('GrapplingHook', {game: game, player: player, socket: socket});

	//pickup manager
	pickups = factory.create('PickupManager', {game: game, factory: factory, socket: socket});
	pickups.setupPickups();
	game.pickups = pickups;

	//"main menu" setup
	addText("-LineRunners-\nWaiting for another\nplayer to connect");

	//speedboosts
	setupSpeedboosts();


	game.input.onDown.add(player.grapplingHook.shootHook, player.grapplingHook);
 	game.input.onUp.add(player.grapplingHook.removeHook, player.grapplingHook);
	player.bringToTop();
	text.parent.bringToTop(text);
	game.reset = function(){
		game.world.remove(text);
		player.reset(x, y, 1);
		pickups.setupPickups();
		gameOver = false;
		resetEffects();
		setupSpeedboosts();
		addText("-Player disconnected-\nWaiting for another\nplayer to connect");
	};

	game.endTheGame = function(){
		console.log("GAME OVER!!!!!");
		gameOver = true;
		//player.canMove = false;
		addText("-GAME OVER-\nYOU"+ winLose + "!\n");
	};
}

function setupSpeedboosts(){
	var speed = game.add.sprite(250+768+148+10-500 + 60+400+16-(13*48), player.y+45-(5*48), 'speedupR');
	speedBoosts.push(speed);
	speed = game.add.sprite(250+768+148+10-500 + 60+400+(18*48)-34, player.y+45-(14*48), 'speedupR');
	speedBoosts.push(speed);
	speed = game.add.sprite(250+768+148+10-500 + 60+400+(18*48)-34+(30*48)+24, player.y+45-(14*48)+(16*48), 'speedupR');
	speedBoosts.push(speed);
	speed = game.add.sprite(250+768+148+10-500 + 60+400+(18*48)-34+(39*48), player.y+45-(14*48)+(16*48)-(17*48), 'speedupR');
	speedBoosts.push(speed);
	speed = game.add.sprite(250+768+148+10-500 + 60+400+(18*48)-34+(36*48), player.y+45-(14*48)+(16*48)-(17*48)+(31*48), 'speedupR');
	speedBoosts.push(speed);
	for (var i =0; i<speedBoosts.length; i++){
		speedBoosts[i].animations.add("runR",[2,1,0],3,true);
		speedBoosts[i].animations.play("runR");
		speedBoosts[i].anchor.setTo(1,0);
		speedBoosts[i].type="boost";
		speedBoosts[i].duration=1.75;
		speedBoosts[i].direction = "r";
		speedBoosts[i].multipler=1.5;
		speedBoosts[i].parent.sendToBack(speedBoosts[i]);
		game.inGameEffects.push(speedBoosts[i]);
	}
}

function addText(script){
	if(text){
		game.world.remove(text);
	}
	var style = { font: "65px Arial", fill: "#FF0000", align: "center"};
	text = game.add.text(player.x, player.y-200, script , style);
	text.anchor.set(0.5);
	text.stroke = '#ffffff';
	text.strokeThickness = 8;
}

function resetEffects(){
	for (var i =0; i < game.inGameEffects.length; i++){
		game.inGameEffects[i].destroy();
	}
	game.inGameEffects.length = 0;
};

function checkInGameEffects(player){
  //console.log("num effects: " + game.inGameEffects.length);
  if(game.inGameEffects.length > 0){
    var boundsA = player.getBounds();
    for (var i = 0; i < game.inGameEffects.length; i++){
      switch (game.inGameEffects[i].type) {
        case "slow":
          var pickEff = game.inGameEffects[i];
          var boundsB = pickEff.getBounds();
          if (Phaser.Rectangle.intersects(boundsA, boundsB) || Phaser.Rectangle.intersects(boundsB, boundsA)){
            player.speedMultipler = pickEff.multipler;
          } else {
            player.speedMultipler = 1;
          }
          break;
        case "boost":
          var pickEff = game.inGameEffects[i];
          var boundsB = pickEff.getBounds();
          if (Phaser.Rectangle.intersects(boundsA, boundsB) || Phaser.Rectangle.intersects(boundsB, boundsA)){
            if(player.direction == pickEff.direction){
              player.speedMultipler = pickEff.multipler;
              game.time.events.add(Phaser.Timer.SECOND * pickEff.duration, function(){
                player.speedMultipler=1;
              }, this);
            }
          }
          break;
        default:

      }
    }
  }
}

function setupBridges(){
	addBridge(250+768+148+10-288, 2616-240, 250+768+148+10+140-(11*48), 2616-48+12-(5*48));
}

function addBridge(bX, bY, lX, lY){
	bridge1 = game.add.sprite(bX, bY, 'bridge');
		game.physics.p2.enable(bridge1);
		bridge1.body.data.gravityScale = 0;
		bridge1.body.setCollisionGroup(groundCollisionGroup);
		bridge1.body.collides(playerCollisionGroup);
		bridge1.body.collides(crateCollisionGroup);
		bridge1.body.mass = 1000000;
		bridges.push(bridge1);
	lever1 = game.add.sprite(lX, lY, 'lever');
		lever1.bridge = bridge1;
		lever1.up = false;
		lever1.timer=0;
		lever1.anchor.setTo(0.5,0.5);
		levers.push(lever1);
}

function hitGround(sprite, tile) {
	player.canJump = true;
}

function endGame() {
	//socket.endGame();
	winLose = " WIN";
	game.endTheGame();
}

function checkLevers(){
	var boundsA = player.getBounds();
	for (var i =0; i <levers.length; i++){
		var lever = levers[i];
		if (lever.timer < game.time.now){
	  	var boundsB = lever.getBounds();

	    if (Phaser.Rectangle.intersects(boundsA, boundsB) || Phaser.Rectangle.intersects(boundsB, boundsA)){
				console.log("player on lever");
				openBridge(i);
				socket.openBridge({lever: i});
			}
		}
	}
}

function openBridge(leverInd){
	var lever = levers[leverInd];
	if (lever.up){
		while(lever.bridge.body.angle > 0){
			lever.bridge.body.angle -=0.2;
		}
		//lever.bridge.body.angle = 0;
	} else {
		while(lever.bridge.body.angle < 90){
			lever.bridge.body.angle +=0.2;
		}
	}
	lever.up = !lever.up;
	lever.scale.x *= -1;
	lever.timer = game.time.now + 1000;
}

function update() {
	if (!Phaser.Rectangle.contains(game.camera.view, player.x, player.y)){
		console.log("player offscreen");
	}

	if(game.otherPlayers.length < 1 || gameOver){
		//stop update loop running until all players
		console.log("game not ready");
	} else {
		if(text){
			game.world.remove(text);
			text = false;
		}
		player.body.velocity.x = 0;
		pickups.checkPickups(player);
		checkInGameEffects(player);
		checkLevers();
		if(player.canMove){
			 if (btnLeft.isDown){
				 if(player.swinging){
					 player.body.force.x	= -25000;
				 } else {
					 player.body.velocity.x = -400 * player.speedMultipler;
					 player.scale.x = -1;
					 player.animations.play("run");
					 player.direction = "l";
				 }
			 }
			 else if (btnRight.isDown) {
				 if(player.swinging){
					 player.body.force.x	= 25000;
				 } else {
					 player.body.velocity.x = 400 * player.speedMultipler;
					 player.scale.x = 1;
					 player.animations.play("run");
					 player.direction = "r";
				 }
			 }
			 else{
				 player.animations.play("stand");
				 //socket.standPlayer();
			 }
			 if (btnJump.isDown) {
				 if(player.canJump){
				 		player.body.velocity.y = -500;
						player.canJump = false;
				}
			 }
				if (pickupUseBtn.isDown) {
					if(player.canJump && !player.swinging){
						if(player.activePickup != null){
							pickups.consumePickup(player);
						}
					}
				}
		}
		socket.setPlayerVelocity({x: player.x, y: player.y, velX: player.body.velocity.x, velY: player.body.velocity.y, direction: player.direction});

		player.grapplingHook.updateSwingLine();
		for(var i =0; i < otherPlayers.length; i++){
			if(otherPlayers[i].getSwinging()){
				otherPlayers[i].updateSwingLine();
			}
		}
	}

}

function render() {
	if (game.drawSwing){
		game.debug.geom(player.grapplingHook.getSwingLine());
	}
	for(var i =0; i < otherPlayers.length; i++){
		if(otherPlayers[i].getSwinging()){
			game.debug.geom(otherPlayers[i].getSwingLine());
		}
	}
}
function quitGame(pointer) {

}
