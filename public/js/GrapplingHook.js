function GrapplingHook(game, player, socket) {
	var sensor = -1;
	var wallAnchor=-1;
	var swinging=false;
	var hookSurf;
	var player = player;
	var game = game;
	var socket = socket;
	wallAnchor = game.add.sprite(0,0, "hookAnchor");
	wallAnchor.bringToTop();
	game.physics.p2.enable(wallAnchor);
	wallAnchor.body.static = true;
	wallAnchor.constraints = [];
	wallAnchor.constraintsCount=0;
	var swingLine;


	this.shootHook = function(sprite, pointer){
		//console.log(player);
		//console.log(player.body);
		//console.log("inputy:" + game.world.input.y);
		//console.log("player.y:" + player.body.y);
		if (game.input.worldY < player.body.y){
			console.log("shooting");
			sensor = game.add.sprite(player.x, player.y  ,'hookAnchor');
			game.physics.p2.enable(sensor);
			sensor.body.data.gravityScale = 0;
			sensor.body.setCollisionGroup(grappleHookCollisionGroup);
			sensor.body.collides(game.hookSurfaceCollisionGroup, this.createSwing, this);
			sensor.body.collides(game.groundCollisionGroup, this.killHookSensor, this);
			sensor.body.rotation=-0.872;
			sensor.target = new Phaser.Point(game.input.worldX, game.input.worldY);
			game.physics.arcade.moveToXY(sensor, sensor.target.x, sensor.target.y, 1500);
			swingLine = new Phaser.Line(0,0,0,0);
			swingLine.anchorPoint = new Phaser.Point(sensor.x, sensor.y);
			swingLine.originPoint = new Phaser.Point(sensor.x, sensor.y);
			game.drawSwing=true;
		}

	}

	this.updateSwingLine = function (){
		if (game.drawSwing){
			if (player.swinging){
				//draw complete swing-line
				swingLine.setTo(player.x, player.y, wallAnchor.x, wallAnchor.y);
			} else {
				//draw increasing swing line
			swingLine.anchorPoint.setTo( sensor.x,  sensor.y);
			swingLine.setTo(player.x , player.y, swingLine.anchorPoint.x, swingLine.anchorPoint.y);
			}
		}
	};

	this.getSwingLine = function (){
		return swingLine;
	};

	this.createSwing = function(){
		player.swinging = true;
		var dist = this.distanceBetweenPoints([player.x,player.y],[sensor.x, sensor.y]);
		wallAnchor.x = sensor.x;
		wallAnchor.y = sensor.y;
		wallAnchor.body.x = sensor.x;
		wallAnchor.body.y = sensor.y;
		wallAnchor.bringToTop();
		wallAnchor.visible=true;
		wallAnchor.body.rotation = -1.570;
		console.log("adding constraint");
		if (wallAnchor.constraintsCount == 0 ){
			wallAnchor.constraints.push(game.physics.p2.createDistanceConstraint(player, wallAnchor, dist, [0,0], [0,0], 1000));
			wallAnchor.constraintsCount++;
		}

		sensor.body.removeFromWorld();
		sensor.kill();
		socket.swingPlayer({anchorX: wallAnchor.x, anchorY:wallAnchor.y});
	};

	this.distanceBetweenPoints = function(pointA,pointB){
	    var dx = pointA[0] - pointB[0];
	    var dy = pointA[1] - pointB[1];
	    var distance = Math.sqrt(dx*dx + dy*dy);
	    return distance;
	}

	this.removeHook = function(){
		for (var i = 0; i < wallAnchor.constraints.length; i++){
			console.log("removing constraint");
			game.physics.p2.removeConstraint(wallAnchor.constraints[i]);
		}
		wallAnchor.constraints=[];
		wallAnchor.constraintsCount = 0;
		player.swinging = false;
		game.drawSwing = false;
		this.killHookSensor();
		socket.removeSwing();

	}

	this.getWallAnchor = function(){
		return wallAnchor;
	};

	this.getSensor = function(){
		return sensor;
	};

	this.killHookSensor = function(){
		if (!player.swinging){
			sensor.kill();
			//game.drawSwing = false;
			wallAnchor.visble=false;
		}
	};


	return this;

}
