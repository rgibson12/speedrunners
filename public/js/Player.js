function Player(id, x, y, game) {
	var id = id;
	var x = x;
	var y = y;
	var lastX;
	var lastY;
	var lastDir;
	var game = game;
	var direction ="r"
	var player = game.add.sprite(x, y, 'character');
	player.bringToTop();
	player.x = x;
	player.y = y;
	player.canMove = true;
	player.activePickup = null;
	game.physics.enable(player, Phaser.Physics.P2JS);
  player.body.collideWorldBounds = false;
	player.animations.add("run",[1,2,3,4,5,6],45,true);
	player.animations.add("freeze",[14]);
	player.animations.add("stand",[0]);
	player.direction = direction;
	player.body.fixedRotation = true;
	player.swinging = false;
	player.swingAnchor = new Phaser.Point(0,0);
	player.swingLine = new Phaser.Line(0,0,0,0);
	//console.log(game.playerCollisionGroup);
	player.body.setCollisionGroup(game.playerCollisionGroup);
	player.body.collides(game.groundCollisionGroup);
	player.body.collides(game.crateCollisionGroup);
	player.body.collides(game.endpointCollisionGroup, game.endTheGame, this);



/*************************************
						   getters
*************************************/
	this.getID = function(){
		return id;
	};

	this.getX = function(){
		return  player.body.x;
	};

	this.getY = function(){
		return player.body.y;
	};

	this.getDirection = function(){
		return direction;
	};

	this.getInGamePlayer = function(){
		return player;
	};

this.getActivePickup = function(){
	return player.activePickup;
};

this.getPlayerTex = function(){
	return player;
};

this.getSwinging = function(){
	return player.swinging;
};

this.getSwingLine = function(){
	return player.swingLine;
};

this.destroy = function(){
	player.destroy();
};

this.updateSwingLine = function(){
	console.log("calling player update swiung line");
	player.swingLine.setTo(player.x, player.y, player.swingAnchor.x, player.swingAnchor.y);
}

/*************************************
					   	setters
*************************************/
	this.setSwinging = function(swinging, anchorX, anchorY){
		console.log("called setSwining with:" )
		if(swinging){
			player.swinging = true;
			player.swingAnchor.setTo(anchorX, anchorY);
		} else {
			console.log("removing swing line");
			player.swinging = false;
			player.swingLine.setTo(0,0,0,0);
			player.swingAnchor.setTo(0,0);
		}
	};

	this.setActivePickup = function(pickup){
		console.log("setting active pickup for: " + this.getID() + "as:" + pickup.getType());
		player.activePickup = pickup;
	};

	this.setID = function(newID){
		id = newID;
	};

	this.setDirection = function(newDirection){
		direction = newDirection;
		player.direction = newDirection;
	};

	this.setX = function(newX){
		x = newX;
		player.x = newX;
		player.body.x = newX;
	};

	this.setY = function(newY){
		y = newY;
		player.y = newY;
		player.body.y = newY;
	};

	this.moveAtSpeedX = function(speed){
		player.body.velocity.x = speed;
	};

	this.moveAtSpeedY = function(speed){
		player.body.velocity.y = speed;
	};

	this.playAnimation = function(ani){
		player.animations.play(ani);
	};

	this.collideWithWorld = function(){
		game.physics.p2.collide(player, game.layer);
	};

	this.move = function(data){
		lastX = x;
		lastY = y;
		lastDir = direction;
		if (Math.round(lastX) != Math.round(data.x) || Math.round(lastY) != Math.round(data.y)){
			this.setX(data.x);
			this.setY(data.y);
			//console.log("lastDir: " + lastDir + ", newDir: " + data.direction);
			if (lastDir != data.direction){
				this.setDirection(data.direction);
				player.scale.x*=-1;
			}
			this.playAnimation("run");
		} else {
			this.playAnimation("stand");
		}
	};

	this.setVelocities = function(data){
		this.moveAtSpeedX(data.velX);
		this.moveAtSpeedY(data.velY);
		this.move({x: data.x, y:data.y, direction: data.direction});
	};

	this.jump = function(data){
		this.moveAtSpeedX(data.x);
		this.moveAtSpeedY(data.y);
		//this.moveAtSpeedX(data.x);
		//this.moveAtSpeedY(data.y);
		this.setDirection(data.direction);
		/*if(data.dir=="r"){
			this.playAnimation("runR");
		} else {
			this.playAnimation("runL");
		}*/
	}

	this.freeze = function(){
		player.loadTexture("iceBlock");
		player.canMove = false;
		game.time.events.add(Phaser.Timer.SECOND * 3, function(){
			player.loadTexture("character");
			player.canMove = true;
			console.log("unfreezing");
		}, this);
	}

	return this;

}
