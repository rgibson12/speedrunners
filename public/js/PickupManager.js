function PickupManager(game, factory, socket) {
  this.pickups =[];
  this.game = game;
  this.factory = factory;
  this.socket = socket;
  this.bullet = false;
  this.pickupCounter = 0;

  this.checkPickups = function(player){
    var boundsA = player.getBounds();
    for (var i = 0; i < this.pickups.length; i++){
      var pick = this.pickups[i];
      if (!pick.getTaken()){
        var boundsB = pick.getPickup().getBounds();

        if (Phaser.Rectangle.intersects(boundsA, boundsB) || Phaser.Rectangle.intersects(boundsB, boundsA)){
          this.givePickup({player: player, pickup: pick});
        }
      }
    }
  };

  this.checkHook = function(player1, player2){
    var boundsA = player1.getBounds();
    var boundsB = player2.getBounds();
    if (Phaser.Rectangle.intersects(boundsA, boundsB) || Phaser.Rectangle.intersects(boundsB, boundsA)){
      this.removeHook(player1, player2);
    }
  }


  this.givePickup = function(data){
    var pickup = data.pickup;
    data.player.activePickup = pickup;
    pickup.setTaken(true, player);
    //console.log("player acquired pickup: " + pickup.getType() + " " + pickup.getID());
    this.removePickup(pickup.getID());
    this.socket.givePickup({pickup: pickup.getID()});
  };

  this.consumePickup = function(player){
    //console.log("consuming pickup at manager level - " + player.activePickup.getType());
    var ret = player.activePickup.consumePickup({player: player, game: this.game});
    player.activePickup = null;
    this.socket.consumePickup({player: ret.player, effectedPlayer: ret.effectedPlayer});
  };

  this.setupPickups = function() {
    for (var i = 0; i < this.pickups.length; i++){
      this.pickups[i].destroy()
    }
    this.pickups.length =0;
    this.pickupCounter =0;

    for (var i =0; i < game.pickupCrates.length; i++){
      game.pickupCrates[i].destroy();
    }
    game.pickupCrates.length = 0;

    var newPickup = factory.createPickup({id: this.pickupCounter, x: 550, y:2575, game:game, type:'toxicSpill', manager: this});
    newPickup.addToGame();
    this.pickups.push(newPickup);
    this.pickupCounter++;

    newPickup = factory.createPickup({id: this.pickupCounter, x: 850, y:2575, game:game, type:'iceBlast', manager: this});
    newPickup.addToGame();
    this.pickups.push(newPickup);
    this.pickupCounter++;

    newPickup = factory.createPickup({id: this.pickupCounter, x: 250, y:2575, game:game, type:'crate', manager: this});
    newPickup.addToGame();
    this.pickups.push(newPickup);
    this.pickupCounter++;

    newPickup = factory.createPickup({id: this.pickupCounter, x: 950, y:2575, game:game, type:'hook', manager: this});
    newPickup.addToGame();
    this.pickups.push(newPickup);
    this.pickupCounter++;
  };

  this.removePickup = function(id){
    var pickup = this.pickups[id];
    pickup.removeFromGame();
  };
	return this;

}
